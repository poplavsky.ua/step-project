let list = document.querySelector('.main-our-work-list');
let items = document.querySelectorAll('.tabs-content-item');
let listTitles = document.querySelectorAll('.main-our-work-item');

function filterProduct (e) {
  let targetOurWorkTabName = e.target.getAttribute('data-our-work-tab-name');
  if (targetOurWorkTabName) {
    let targetTitle = e.target;
    listTitles.forEach(listTitle => listTitle.classList.remove('active'));
    targetTitle.classList.add('active');
  }
  if (targetOurWorkTabName === 'all') {
    items.forEach(item => {
      item.style.display = 'block';
    })
  } else {
    items.forEach(item => {
      if (item.getAttribute('data-our-work-tab-name') === targetOurWorkTabName) {
        item.style.display = 'block';
      } else {
        item.style.display = 'none';
      }
    })
  }
}

list.addEventListener('click', filterProduct);

document.querySelector('.main-our-work-btn').addEventListener('click', event => {
  document.querySelector('.main-our-work-btn p').style.display = 'none';
  document.querySelector('.main-our-work-btn span').style.display = 'flex';
  setTimeout(loadMore, 2000);
})

function loadMore () {
  let btn = document.querySelector('.main-our-work-btn');
  let hiddenContent = document.querySelector('.tabs-content-load');
  btn.style.display = 'none';
  hiddenContent.classList.add('active');
}