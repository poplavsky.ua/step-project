document.querySelector('.main-our-services-item').classList.add('active');
document.querySelector('.main-our-services-text-item').classList.add('active');

function selectContent (e) {
  let target = e.target.dataset.ourServicesTabName;
  document.querySelectorAll('.main-our-services-item, .main-our-services-text-item').forEach(el => el.classList.remove('active'));
  e.target.classList.add('active');
  document.querySelector('.' + target).classList.add('active');
}

document.querySelectorAll('.main-our-services-item').forEach(el => {
  el.addEventListener('click', selectContent);
})