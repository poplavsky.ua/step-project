let currentSlider = 0;
let next = document.querySelector('.main-people-say-slider-btn-right');
let back = document.querySelector('.main-people-say-slider-btn-left');
let navigation = document.querySelectorAll('.main-people-say-slider-photo');
let slides = document.querySelectorAll('.main-people-say-item-user');

function selectReview (e) {
  let target = e.target.closest("li");
  let dataArr = e.target.closest("li").getAttribute("data-people-say-name");

  document.querySelectorAll(".main-people-say-slider-photo, .main-people-say-item-user").forEach(el => {
    el.classList.remove("active");
  })

  target.classList.add("active");
  slides.forEach((review, i) => {
    if (review.getAttribute("data-people-say-name") === dataArr) {
      currentSlider = i;
      review.classList.add("active");
    } else {
      review.classList.remove("active");
    }
  })
}

navigation.forEach(el => {
  el.addEventListener("click", selectReview);
})

back.addEventListener("click", () => {
  if (currentSlider === 0) {
    currentSlider = slides.length - 1;
  } else {
    currentSlider--;
  }
  slides.forEach((review, i) => {
    review.classList.remove("active");
  });
  slides[currentSlider].classList.add("active");
  navigation.forEach((element, i) => {
    element.classList.remove("active");
  });
  navigation[currentSlider].classList.add("active");
})

next.addEventListener("click", () => {
  if(currentSlider === slides.length - 1) {
    currentSlider = 0;
  } else {
    currentSlider++
  }
  slides.forEach((review, i) => {
    review.classList.remove("active");
  });
  slides[currentSlider].classList.add("active");
  navigation.forEach((element, i) => {
    element.classList.remove("active");
  });
  navigation[currentSlider].classList.add("active");
})